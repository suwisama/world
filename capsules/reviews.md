[Index](index.md)

# Media reviews


## Anime
[Serial Experiments Lain](reviews/serial-experiments-lain.md) 
[Initial D S1 and S2 (anime)](reviews/Initial-D-1-2.md)  
[Quintessential Quintuplests](reviews/Quintessential-quintuplets.md)  
[Cells At Work S2](reviews/Hataraku-Saibou-S2.md)  
[Cells At Work](reviews/Cells-at-work.md)  
[Cube X Cursed X Curious](reviews/C3.md)  
[Citrus](reviews/Citrus.md)  
[Nisekoi](reviews/Nisekoi.md)  
[Onii-chan wa Oshimai](reviews/Onii-chan-wa-Oshimai.md)  

## Light Novels
[light novel review Reborn as a Space Mercenary V3](reviews/Reborn-as-a-Space-Mercenary-V-3.md)  
[Gimai-Seikatsu V8 Light Novel](reviews/Gimai-Seikatsu-V8.md)  
[Re:Zero V16 Light Novel](reviews/ReZero-V16.md)  
[Sword Art Online Aincrad V1](reviews/Sword-Art-Online-Aincrad-V1.md)  
[Words Bubble Up Like Soda Pop](reviews/Words-Bubble-Up-Like-Soda-Pop.md)  
[The Game Master Has Logged In to Another World V01](reviews/The-Game-Master-Has-Logged-In-to-Another-World-Volume-1.md)  
[Rebuild World V02 P02](reviews/Rebuild-World-Volume-2-Part-2.md)  
[Is It Wrong to Try to Pick Up Girls in a Dungeon V08](reviews/Is-It-Wrong-to-Try-to-Pick-Up-Girls-in-a-Dungeon-Vol8.md)  
[Is It Wrong to Try to Pick Up Girls in a Dungeon V07](reviews/Is-It-Wrong-to-Try-to-Pick-Up-Girls-in-a-Dungeon-Vol7.md)  
[Is It Wrong to Try to Pick Up Girls in a Dungeon V06](reviews/Is-It-Wrong-to-Try-to-Pick-Up-Girls-in-a-Dungeon-Vol6.md)  
[Is It Wrong to Try to Pick Up Girls in a Dungeon V05](reviews/Is-It-Wrong-to-Try-to-Pick-Up-Girls-in-a-Dungeon-Vol5.md)  
[Spirit Chronicles V13](reviews/Seirei-Gensouki-Spirit-Chronicles-Volume-13-Two-Amethysts.md)  
[Spirit Chronicles V12](reviews/Seirei-Gensouki-Spirit-Chronicles-12-Battlefield-Symphony.md)  
[Spirit Chronicles V11](reviews/Seirei-Gensouki-Spirit-Chronicles-Volume-11.md)  
[Spirit Chronicles V10](reviews/Seirei-Gensouki-Spirit-Chronicles-Volume-10-Forget-me-not-of-Rebirth.md)  
[Spirit Chronicles V09](reviews/Seirei-Gensouki-Spirit-Chronicles-Volume-9-Heroes-in-the-Moonlight.md)  
[Death March to the Parallel World Rhapsody V02](reviews/Death-March-to-the-Parallel-World-Rhapsody--Volume-02.md)  
[Death March to the Parallel World Rhapsody V01](reviews/Death-March-to-the-Parallel-World-Rhapsody--Volume-01.md)  
[Rebuild World V02 P01](reviews/Rebuild-World-Volume-2-Part-1.md)  
[Rebuild World V01 P02](reviews/Rebuild-World-Volume-1-Part-2.md)  
[Rebuild World V01 P01](reviews/Rebuild-World-Volume-1-Part-1.md)  
[ReZero V16](reviews/ReZERO-Starting-Life-in-Another-World-Vol16.md)  
[ReZero V15](reviews/ReZERO-Starting-Life-in-Another-World-Vol15.md)  
[ReZero V14](reviews/ReZERO-Starting-Life-in-Another-World-Vol14.md)  
[ReZero V13](reviews/ReZERO-Starting-Life-in-Another-World-Vol13.md)  
[ReZero V12](reviews/ReZERO-Starting-Life-in-Another-World-Vol12.md)  
[ReZero V11](reviews/ReZERO-Starting-Life-in-Another-World-Vol11.md)  
[ReZero V10](reviews/ReZERO-Starting-Life-in-Another-World-Vol10.md)  
[ReZero V09](reviews/ReZERO-Starting-Life-in-Another-World-Vol9.md)  
[ReZero V08](reviews/ReZERO-Starting-Life-in-Another-World-Vol8.md)  
[ReZero V07](reviews/ReZERO-Starting-Life-in-Another-World-Vol7.md)  
[ReZero V06](reviews/ReZERO-Starting-Life-in-Another-World-Vol6.md)  
[ReZero V05](reviews/ReZERO-Starting-Life-in-Another-World-Vol5.md)  
[ReZero V04](reviews/ReZERO-Starting-Life-in-Another-World-Vol4.md)  
[ReZero V03](reviews/ReZERO-Starting-Life-in-Another-World-Vol3.md)  
[ReZero V02](reviews/ReZERO-Starting-Life-in-Another-World-Vol2.md)  
[ReZero V01](reviews/ReZERO-Starting-Life-in-Another-World-Vol1.md)  
[God's Games We Play V02](reviews/Gods-Games-We-Play-Vol2.md)  
[God's Games We Play V01](reviews/Gods-Games-We-Play-Vol1.md)  
[King's Proposal V02](reviews/Kings-Proposal-Vol2.md)  
[Boku wa Isekai de Fuyo Mahou V02](reviews/Boku-wa-Isekai-de-Fuyo-Mahou-to-Shoukan-Mahou-wo-Tenbin-ni-Kakeru-Volume-2.md)  
[Boku wa Isekai de Fuyo Mahou V01](reviews/Boku-wa-Isekai-de-Fuyo-Mahou-to-Shoukan-Mahou-wo-Tenbin-ni-Kakeru-Volume-1.md)  
[Tomorrow I Will Die,You will revive](reviews/Tomorrow-i-will-die-You-will-revive.md)  
[Infinite Dendogram V03](reviews/Infinite-dendogram-V3.md)  
[Spirit Chronicles V08](reviews/Seirei-Geitsuki-Spirit-Chronicles-Vol-8.md)  
[No Game No Life V01](reviews/V1-No-game-No-life.md)  
[King's Proposal V01](reviews/Kings-Proposal.md)  

## Games
[Ginka](reviews/Ginka.md)  
[Digital:A Love Story](reviews/Digital-a-love-story.md)  
 

## Movies
[Avatar The Way of Water](reviews/Avatar-The-way-of-water.md) 

## Music
- Can't stand you - GUMI