[Home](../index.md)

📅**First written:** 2023-12-08  
📅**Last Updated:**  

# Copyright
## Piracy

In the media, piracy is often portrayed as stealing, there was thing that the US did in before i was even born, that campaign telling you that, "You wouldn't download a car".

["You wouldn't download a car"](https://en.wikipedia.org/wiki/You_Wouldn%27t_Steal_a_Car)  

Piracy shouldn't even have to exist, for ages, humans shared ideas between each other without copyright, and society thrived. But now, with the advent of copyright, progress is actually stalling.

For example, in this book i pirated and lost, there was a example about the invention of the steam engine. The guy that invented it, patented it and didn't allow others to improve on it, later after the patent expired, a lot of major improvements where made to the engine.

The inventor and his company spent more time and money on fighting others in court for making his precious engine better that actually improving it.

> TODO: Add sources

Another example was the 3D printer, i have seen a video on it some years ago but it went something like this.

The inventor of the 3D Printer couldn't find someone willing to produce it, so the invention just sat there and collected dust without any improvements.

After the exclusive term expired, till now a lot of advancements have been done in this field.


There are countless examples of this, copyright stifles creativity and innovation, the exact opposite of what is want's to boost.

The music industry has something similar, there are a couple of companies that just gather the copyright for songs and they ask for a ton of money to allow their use.

### A better solution?
The problem most often brought up is that if there is no copyright, how are the creators supposed to make a living?

The answer is open-source, or something is the spirit of it.

You release your work, and copyright doesn't exist so anyone can copy it, i think a good compromise would be that you are required to mention the original author.

The audience, if they like the song, can go to the original author and donate. Our culture will
slowly change into something like, if you can afford it and like the work, donate to the author.

But this is still not a complete solution. To complete it, society needs to realize that we should't all have to work, not traditionally at least. We should institute a UBI (Universal Basic Income) and free housing, we have more than enough money for this i think.

The idea is that if you are a artist, indie game developer or just want to live your life doing something unconventional, you live of this UBI with some additional income from donations, those who want to work, can work, and if the UBI is just enough for someone to live but not enough for someone to live in luxury, there will be enough incentive for people to work.

I even doubt that there will be many who will want to just sit around, humans get bored easily, and some have dreams. Ether way, the automation and A.I. that is coming up will force us to institute a UBI or risk some heavy consequences, just look at the game Detroit Become Human.