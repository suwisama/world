[Home](../index.md)

📅**First written:** 2023-12-22  
📅**Last Updated:**  

# LGBT
This is a very hot take on LGBT by me...

## Preferences
I often think about things that maybe don't matter but it's fun. And when thinking and analyzing my life till now, i have observed that preferences change. This whole thing translates super well to the LGBT idea.

I think of a gender "not assigned at birth" being a preference not anything more mystical.

Before the popularization of this movement, i believe that the reason homosexuality wasn't spread was that culture made us think of it as a disorder, and of course who would want to be thought as crazy?

From the little information i know, it wasn't that big of a deal in the older times, i meant the discrimination.

People love to polarize , something that's accelerated by the global nature of the internet.

The point i want to make is, you can today be straight, the next month gay, and after two years trans. It isn't something fixed as it's only, and it might sound rude but i will just say it, it's only an illusion of permanence, every type of preference is easy to change.

## Spreading LGBT
Another rude statement but, LGBT is similar to a virus in my opinion, i have no hate for the people, i even think that this proves how complex and marvelous our brain truly is.

> LGBT is a virus

Here you go, i said it.

Now let me explain.

Because of the psychological/illusionary way we assign this gender to us, the more people hear about it, the more it will spread.
Similar to self-diagnosis on the interned for some sickness,if the only thing that separates a "normal" person and a LGBT person it the "you just feel like it", you can feel like it after listening to all these people telling you it's something normal, even if you didn't ever have any other attraction to gender abnormalities.

So you can ask someone if he has ever felt attraction to the same gender as himself for example, and even if he never felt it he might say, "Now that i think about it, i did".

The brain is a very elaborate machine that  can't be trusted with memories, it spins  them and muddles and changes them that when you read your journal after some years you will be surprised by the difference between your memory and the facts.

## Asexuality
I somewhat think of myself as asexual, but how did i get here, well...
One night before sleep i was thinking that humans have sex for pleasure, in a way betraying the goal our biology gave us, to procreate, i felt bad about it. 

I stopped thinking about it, but after some time my preferences on the matter change to where now i can somewhat consider myself asexual.


## Trans
Now this is a weird topic, in a way we idealize the opposite gender because most of the time we know nothing about how would it feel to be it, so things like tomboys and femboys appear, they are a way of feeling, if just for a bit how the other gender feels by comporting and dressing alike them.

Transitioning is a more extreme idea that probably evolved from that. It involves altering your body trough surgery and or hormone therapy.

Now not all trans people go trough it, but the ones that do are in my opinion stupid.

You are willingly breaking your perfectly functional body just to feel a bit more like the opposite gender that's so mysterious.

If you do the surgery, bye bye to any hope of having kids again, at least normally, but even the hormonal therapy fucks your body a lot.

If you really want to transition, then you are out of luck, until we can make clones and upload our minds from one body to the other there is no way you can be the opposite gender.

Another rude one, but trans people should realize that it's impossible for them to change their gender. I know it's a hard pill to swallow, the realization that there can be no free will broke me down and it felt horrible, but i got over it in about an hour :)


## The feeling of belonging to an label
I see this in myself as well, people tend to assign labels to themselves and try to stick to them as much as possible. This phenomenon means that we assign permanence to qualities about ourselves like liking sweets or being introverted or extroverted, the same is true for this LGBT stuff.

Now this is super off topic so i will just end my post here for now.