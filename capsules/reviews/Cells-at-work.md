# Cells at Work
Hataraku Saibou
Rating:12/10
Date:2023-05-26

## Opinion 
Ahh, this became my favorite anime in the first five minutes. Firstly, i am amazed by the concept, basing an anime around the human body, more specifically it's cells.
The story follows The Red Blood cell
And the White Blood Cell, these characters are very well designed, they are the personalized versions of cells. All the characters are very interesting even the viruses, i am confused by why did them make the platelets be kids but they are very cute 🥺🥰.

I think this anime clearly describes how awesome our body is, being able to deal with such a vast amount of problems, and the great collaboration between the cells.

Idk what to tell more than that this anime is just awesome. This time i didn't write a summary because i am lazy.