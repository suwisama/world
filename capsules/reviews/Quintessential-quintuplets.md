# Review: Quintessential quintuplets
Date: 2023-05-23
Rating: C+

## Summary
The story starts with Futaro meeting with a girl in the cafeteria, she sits bedside him and asks him to tutor her, after she sees his grades and finds he has full marks, he is embarrassed by that,he refuses to tutor her, and gets into an argument.

The next day, he meets 5 girls, his sister informs him that he has a job for tutoring that pays 5 times the usual amount, something that would get them out of debt. He goes to his job and finds that the 5 girls he meat earlier are the ones he has to tutor including the girl he got into an argument with, they are quintuplets.

The girls do not like to study, each are good at one thing, and after he hardly gathers them together, with Yotsuba's help, to take a test, all them combined make 100 points.

Some stuff  happens
, like him getting closer to Miku, learning about why Nino hates him the quintuplet's dad says that if they don't pass the next test he gets fired, the girls dont pass and Itsuki(might get it wrong) lies so that he doesn't get fired.

They go to a festival with his sister, and the girls get separated, while he tries to bring them together he finds about the older sister's secret that she is an actress.

The school trip comes a d so dose his sister's fever. He thinks about the girl he loved that made him change from a delinquent to a study nerd and we get a hind that she was one of the five. Nino sees him in the past and falls in love with that version of him.

At the school trip, he and the oldest girl get stuck in a room, and both get sick.
They go skiing and they find that a girl is missing, Futaro fails to ski 🎿 and Nino sees the bandage that fell from him and thinks its the other guy Futaro was playing as his older self. He finds out that the missing girl actually disguised herself and he gets very sick.

They finally hold his hands when the fireplace starts and fufill the prerequisite of the legend saing that those that hold hands at that time, will be together forever.

And at half the last episode, we see him getting married.

## Opinion 
I think that the quintuplet idea is a fresh one and that the story is solid but the part that's kinda confusing is the end, where when suddenly switch things up, even through we learn about the details later. His sister grown up confused me very much, at least a transition saying "3 years later.", or something would be better.

The characters are very well made but i would have liked a bit more development.

In general, it's a pretty average romance slice of life that has a original idea, but the relationships are a bit too forced in my opinion. 

