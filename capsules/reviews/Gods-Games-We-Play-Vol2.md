# Gods’ Games We Play. Vol. 2
**Author:** Kei Sazane

⭐ **Rating:**  9.1/10

📅 **Date:** 2023-07-05

## Opinion
As with the last one, i did really like the book, yesterday i got to writing my own but i have some troubles with it so i finished this one.

It's kinda weird how the volume ended with a peek to the next volume's first game, it left me confused.

I liked how they added a real life battle and the story with Nel.

As always, i think the author is a genius to be able to come up with these plots for winning the games and the ideas behind them, as to now, every game was rather unique and i love it.