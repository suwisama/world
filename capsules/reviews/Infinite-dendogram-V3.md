# Infinite dendogram V3
📅 Date: 2023-06-23
Rating: 7.2/10 🐢(Turtles)

## Review 
I find the series Interesting, way more  that the anime that i have watched about 2 years ago. The thing is that the idea is solid but Sakon Kaidou's writing is lacking.

The most annoying part is the battle pacing, it doesn't have any good feel to it, it feels like a glorified todo list. I might be harsh but i guess Sakon-san will not get to read my rambling. The second anoyence is how much they talk about how the game works, this is something that should be cut down a little.

I gave it  7.5 turtles because of the idea otherwise it would be a 5.

## Summary 
This volume starts with a introduction of Xunyu if i got her name right. The volume basically consists of Ray, Mari Shu and Rook and their embryos especially Nemesis going to watch the battle between 2 superior Masters, Xunyu and Figaro. The plot is kinda cut short, and will be continued in volume 4, by 2 side stories, they reveal Mari as being a Mangaka that joined the game to roleplay as her MC and how she is actually the one who gave Ray his first Death penalty.