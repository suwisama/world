
This is a short story I wrote as part of a school homework I went overboard with. It sucks but I want to have it here to have a record of how I improved.

I wrote the scenes but had ChatGPT rewrite them, so yeah!

Enjoy!

# Cryptic Shadows

On a dark summer night, the full moon cast a soft glow in Finn's room. Pausing his book, he noticed a family photo on his bedside table, illuminated by the moonlight.

In the picture's center, Finn stood with his older sister, Ava, her bright smile lighting up the frame. It felt like a warm memory. On one side was their mother, Chloe, in a light blue dress and hat, and on the other, Finn and Ava's father, a tall, handsome man with a grin.

Finn smiled at the old photo, taken during a beach trip two years ago—the last one with his sister, who had passed away in an accident a few months later.

Since then, their family had been struggling. His mom turned to drinking to numb the pain, and his dad became distant, working late and sometimes not coming home for days.

Choosing a night stroll to escape his haunting memories, Finn left his room and closed the door quietly. The only light in the house emanated from the forgotten TV. He found his mother passed out on the sofa.

Taking the remote from her grasp, he discovered a blanket nearby and covered her gently to keep her warm. A moment of hesitation struck him before turning off the TV. The news report displayed gruesome details about a series of strange murders in the city. Victims were discovered with fang marks on their backs or their heads gruesomely severed, discarded nearby. Despite the TV station's blur, the horrific scene was hard to miss.

Inhaling deeply, Finn switched off the TV and departed from the house. He looked up at the night sky, hands in his pockets, and contemplated the full moon shining brightly above.

The young boy, couldn't even fathom the terrors that would haunt him that night.

  
  

After a short walk in the neighborhood, Finn reached the park. The breeze brushed lightly against his melancholic face, and the streetlights illuminated his way.

In the distance, he spotted a familiar figure. He quickened his steps and approached the man.

"Dad, why are you here again?" Finn asked, but his father remained silent.

"Dad?" he inquired once more. Finally, his father turned to face him, but something was off. His face bore signs of fatigue, with dark shadows under his eyes and a blank expression, his eyes bloodshot.

"Dad, are you alright?" His father's mouth moved, but his words were barely audible, leaving Finn bewildered.

"Finn... R-r-r... run," his father's voice was hoarse and incomprehensible. Finn stood frozen, just inches away from his father.

Suddenly, the man began to transform, growing larger and more powerful by the second. Black hair sprouted from his body, his clothes tore, and his hands turned into fangs. Finn couldn't even scream. He felt a searing pain in his stomach and realized he was bleeding. He clutched his hands over the wound, his legs giving way, and he collapsed to the ground.

The werewolf, with a sorrowful look in his eyes, whispered five words in Finn's fading consciousness.

"I am sorry for everything."

  
  

  
  

Finn's return from the depths of darkness was accompanied by a putrid stench so foul it could curdle one's stomach. He clambered to his feet, disoriented as his surroundings shifted in the pitch-black void. At the onset, they found themselves in a dark, old, and abandoned, crumbling conference room.

A cacophony of questions flooded his mind, a torrent of uncertainty: "How am I still alive? Was it all just a nightmare? What happened to my father? Where in the world am I?"

A playful, girlish voice cut through his confusion, almost indifferent to his distress. "Well, it seems I've succeeded in reviving you. You'd better prove useful."

"Who are you? Where the hell am I?" Finn demanded, his voice trembling.

The girl sighed in exasperation, though she remained concealed in the shadows. "I've given away a significant part of my life force for you, and you're still ungrateful?" A sense of dread washed over him, and soon, he lost control of his own body.

"What are you doing to me? What's happening?" Finn cried out.

  
  

"Calm down," the girl said, reaching for a nearby light switch and flooding the room with blinding light. Finn shielded his eyes from the sudden brilliance. "I borrowed your body temporarily. Unfortunately, you can't see in the dark like me."

For the first time, Finn's gaze fell upon the girl. She was undeniably beautiful, with pale skin, long brown hair, and a petite figure. She appeared to be about sixteen, his own age, but there was something peculiar about her. Her entire body seemed misty, almost transparent, much like the ghosts he'd read about in his beloved horror novels.

"See," the girl continued, pointing towards a corner of the cramped room, "that's my body. In this state, I can't touch anything, but I can possess objects and other living beings."

Reluctantly, Finn turned his gaze to where she was pointing. The corpse lying there could have been her twin, but unlike her, it had reached an advanced stage of decomposition. Maggots writhed through rotting flesh, rodents scurried about, and a foul stench hung in the air.

Finn's stomach turned, and he couldn't contain the nausea any longer. He vomited on the floor. "What happened to you? How did this happen?"

The girl giggled nervously. "Well, you see... ehehehe. You know the werewolf that killed you? He killed me too a few days ago and brought me here. I could show you the other rooms; there are more corpses."

Finn struggled to comprehend the girl's apparent indifference to her own gruesome situation. Her lifeless body lay right in front of him, swarmed by insects and decomposing, yet she spoke of it as if it were a mere joke. He quickly averted his gaze as the sickness threatened to resurface.

After regaining some composure, he asked, "What's your name?"

The girl remained silent, not responding to his question.

The room itself, still drenched in the cold, blinding light, seemed to crumble before their eyes. The once-grand conference room was now a decaying chamber of memories, its walls marred by the ravages of time. The wooden panels that once lined the walls now hung askew, and the ceiling sagged ominously, bearing the weight of forgotten secrets.

The echoes of their conversation bounced off the cracked and faded wallpaper, creating an eerie, hollow feeling that pervaded the space. Dust motes danced in the air, stirred up from years of neglect. It was a place where the remnants of forgotten meetings and whispered conspiracies whispered through the cracks, still lingering in the shadows.

  
  

In his desperation for answers, Finn ventured one more time, his voice trembling as he inquired, "Can you at least tell me your name?"

But the girl's ethereal form remained silent, her identity shrouded in an impenetrable veil of secrecy.

The room seemed to absorb his question, giving no response, and the stench of decay lingered in the air, a constant reminder of the gruesome reality that surrounded them. His unease grew, an ever-present companion in this desolate place.

The girl, still draped in her ethereal aura, glanced at him with a mix of curiosity and a hint of sorrow, as if she longed to provide answers but was bound by some mysterious constraint. Her inscrutable silence left Finn with a gnawing sense of dread, an unresolved puzzle in an already nightmarish scenario.

  
  

  
  

Finn had momentarily let his guard down, a lapse that, in their dire circumstances, proved to be a potentially fatal mistake. A thunderous roar erupted from the hallway, instantly silencing the children in the dimly lit conference room. In his fear, Finn's trembling hand knocked over a chair, sending it crashing loudly to the floor. The sound reverberated through the dark, abandoned facility, like a sinister symphony of doom.

In response to the noise, the monstrous being beyond the door sprang into action. It kicked the door open with a horrifying display of strength, exposing the lone exit within the room. Panic surged through Finn, causing his heart to race and his breath to turn shallow. The grotesque creature was closing in, and there was nowhere left to hide.

But just as despair threatened to overtake him, Finn suddenly felt a familiar presence, a consciousness intruding into his mind—it was the ghostly girl. It was she who acted, guiding him, lending her ethereal abilities to ensure their shared survival. With her influence, he seized a chair and hurled it with precision directly at the approaching monstrosity. The chair struck the grotesque creature, causing it to momentarily stumble. But it quickly regained its balance and continued its relentless advance.

The noxious stench from the creature's gaping maw intensified, and its numerous appendages loomed ever closer. The room seemed to constrict around Finn, and terror gnawed at his chest as he realized there was no escape.

With newfound determination and the spectral girl's mysterious support, Finn acted decisively. He sprinted around a table, snatched up a chair, and made a dash for the door. With the chair in hand, he barricaded the exit and struggled to push a nearby cabinet against the door, creating a makeshift barrier.

Exhausted and trembling, Finn and the enigmatic girl huddled against the wall on the other side of the barricade. The terror that had gripped them moments before was now replaced by fatigue.

"Man! That was intense. Hahaha," the ghost's voice quivered, a mix of relief and lingering fear.

As his fear slowly abated, Finn's anger resurfaced, and he turned to the ghostly girl. "How can you be so excited? I was about to die! Do you understand?"

Finn continued, "You possessed my body too."

Finn's frustration ebbed as he remembered their unique predicament, and he was overwhelmed with guilt for having lashed out at his savior but his pride didn’t allow him to apologize.

The room remained quiet, save for the distant echoes of the underground facility, a labyrinth filled with untold horrors, as they confronted the unknown together.

  
  

After their nerves settled a bit, Finn inquired, "So, what's our next move?"

"Well, we need to press on," the ghost replied. "You want to escape from here, don't you?"

"I sure do, but I'm also terrified," admitted Finn, still struggling to comprehend the girl's unwavering confidence.

“You see," the girl explained, "unlike you..." She cast a teasing glance at his body. "My body's strength isn't akin to that of a kitten's purr."

"Hey!" Finn interjected, feeling a mixture of surprise and indignation.

She continued, "I've been trained in martial arts since I was a child. I know a thing or two about fighting."

After a brief pause, the ghost girl's demeanor shifted, and she asked, "So, are you in, or do you want to remain here like my lifeless body?"

Recalling the grisly state of the ghost girl's body, fear surged through Finn. "I'm in," he stammered, determination mingling with trepidation. "So, what do I need to do?"

  
  

"Nothing," she responded, a hint of mischief in her voice. "Just don't complain when I use your body." Her penchant for teasing remained an enigma to him, leaving him both intrigued and puzzled.

  
  

Finn and the ghost girl stood in the hallway, with a turn to the right not too far ahead.

"There's an armory around the corner," the ghost girl explained. "It seems the security personnel used it in the past. Besides some knives, there's not much there. And even if we were to find guns, using them would be a bad idea."

Finn, curious, inquired, "Why can't we use guns? Aren't they stronger and safer to use?"

The ghost girl smiled warmly but couldn't help thinking that Finn had a few screws loose. "Do you want to host a monster party? A gunshot would attract everyone in the facility to your location. I doubt you'd like that."

Finn nodded silently, understanding the rationale.

"Then let's go," Finn declared, eager to escape from their nightmarish predicament. He stood up and began to walk forward.

The ghost girl couldn't help but facepalm herself, even though no sound was made. Finn, noticing her reaction, turned to see her in that state.

"We need to be sneaky. Where do you think you're going?" she cautioned.

Finn cast a cautious glance around the dimly lit room, realizing that he had to tread carefully in this grim place. The stench of decay clung to the air, and the faintest sound could be his undoing.

The ghost girl crouched down and whispered sweetly in his ear, "Follow me."

Finn followed suit and quietly trod the path behind the ghost girl. When they reached the corner, the ghost girl signaled for him to stop. He looked bewildered but decided that continuing on recklessly was not the way to go.

After a while, she started walking again, and Finn, curious, asked, "Why did we stop?"

The ghost girl gestured silently to a giant spider blocking their path. They continued walking cautiously.

  
  

  
  

  
  

  
  

As they reached the armory, they quickly used chairs and furniture to block the path behind them before entering to search for supplies. The room was relatively small and appeared cleaner than the others, as if no monsters had breached its confines. Everything was scattered about haphazardly, indicating that the previous inhabitants had hastily evacuated.

Finn picked up one of the knives from a table and idly played with it. "There's nothing useful here. I wouldn't want to fight with these knives."

"Be careful," the ghost girl whispered. "We have some medicine here, but it's old. We don't want any unnecessary injuries." Realizing his earlier carelessness, Finn quickly put the knife down.

The same unusual yet now somewhat familiar sensation enveloped him as his body was possessed by the ghost girl. She aligned her ghostly form with his, taking control. She picked up a knife and skillfully used a chair leg to fashion a makeshift spear, securing the knife in place.

Curious, Finn asked, "What are you trying to do?"

She explained, "A spear is better than a knife." She then found a black uniform in the cupboard, which roughly fit him.

"What's that for?" Finn inquired.

"Did you forget that your body was torn? And anyway, this will offer better protection." She also handed him a bulletproof vest from the cupboard and instructed him to change into it.

After gearing up, Finn and the girl continued their escape. They were walking through an internal garden with no access to the sun, only scant light filtered through. The walls were covered in moss, and vines clung to the distant ceiling. Speaking of the ceiling, it was a glass dome, its glass shattered, and the frame overrun by vegetation. Dim artificial lights flickered above it.

The scent of the verdant garden provided a welcome reprieve from the rot that pervaded the other hallways, but it posed a new challenge to traverse.

Finn, somewhat sarcastically, exclaimed, "So lovely, you'd think we went out for a stroll."

The ghost, now in control of Finn's body, cut a path forward through the bothersome vines. "Ugh, these vines are so annoying," she complained. "We're headed to the other side; that's where we'll reach the exit room."

Curious, Finn inquired, "How much farther do we have to walk?" The girl sighed, "A long, long way. These vines... I almost miss being a ghost." Her voice conveyed annoyance, a rare tone from her, and though Finn couldn't see her expression, he couldn't help but grin upon hearing the irritation in her voice.

They continued to chat about random topics for a while, almost forgetting the perilous nature of their surroundings. Unbeknownst to them, a small fox was following their trail.

Upon reaching the grand oak tree in the center of the garden, they were greeted by the sight of a little fox. However, this fox swiftly transformed into a striking woman, her red hair, fox ears, and nine tails giving her an appearance of cuteness—until her sharp fangs and bloodstained face were revealed. A foul stench emanated from her mouth, a noxious scent of rotten flesh, and blood dripped from her chin onto her once-white dress. Her fangs were stained with blood, though she attempted to conceal them.

As they stood before her, the fox-woman let out a chuckle and then disappeared, not before slashing her hands at Finn's stomach and shattering his thick bulletproof vest.

Finn felt as if his heart stopped for a second as terror surged through his body. He quickly turned to the ghost girl, his voice shaking. "What the hell was that!?"

The girl responded, her voice tinged with confusion, "That's a Nine-Tailed Fox, also known as a kitsune, a being from Asian mythology. But I've never seen one here..."

Drenched in cold sweat, Finn exclaimed, "We should get out of here as quickly as possible," and discarded the now-useless vest.

When he turned to glance back, he found the wall painted with fresh blood, bearing a chilling message: "I'm watching you." It was a menacing warning from the Kitsune.

  
  

  
  

The kids continued their trek, now with a heightened sense of worry. Finn reached into his backpack, retrieving an expired energy bar and a bottle of water. He stopped to rest, but this would be the second mistake he made. After consuming the meager rations, he settled onto a nearby stone, his eyelids growing heavier and heavier with each passing moment until he finally succumbed to slumber.

"Finn, wake up! Finn…" The ghost girl called out urgently, attempting to rouse him. However, his face remained placid, as if he were experiencing the most pleasurable dream, and drool accumulated beneath his head.

"Finn, if you don't wake up, you may never get another chance to escape. This could be your last opportunity!" The ghost girl's usually pretty face was now consumed by terror, her heart racing as the Nine-Tailed Fox drew near.

The crimson vixen approached slowly, touching the boy gently and lifting him in her hands. She caressed his hair and handsome face with her bloody fangs. All the while, the ghost girl continued her efforts to awaken him, but in his current state, she couldn't possess his body.

"No, don't take him!" The fox woman responded with nothing more than a villainous laugh.

The ghost girl attempted to pry Finn away from the fox's grasp, but her ethereal state proved futile. In desperation, she remembered something and seized control of a cluster of vines, hurling them at the fox in a frenzied attack. However, they made no difference; the Kitsune either shredded them with her fangs or nimbly dodged them.

"Die, die already!" the girl cried out, her face contorted with anguish.

"You and the evil scientists who worked in this facility killed me and my entire family," she yelled, possessing more vines and flinging objects at the fox—glass shards, chairs, rocks. She took control of the plants, releasing their thorns. Even the skeletal remains of the scientists became her warriors.

"Building this underground bunker to win the Second World War, creating vile creatures, kidnapping children to inject them with drugs," she gasped, her voice trembling.

"You took my older brother, the one I loved so much, put him in this lab, turned him into a monster," she lamented, remembering his handsome smile, his kindness, and the horrors he'd become.

  
  

Her brother, transformed into a monstrosity with limbs elongated to two meters each, his muscles torn and exposed, countless metal rods and electronic components meant to sustain his staggering height. Empty eye sockets, a bloody and broken face.

After government officials had come to their house, they'd tried to take her brother away. She and her parents had fought fiercely to keep him, but their efforts had been in vain. They had been killed, and her brother had been taken from them. She had been left to fend for herself, living on the brink of survival, hidden in a closet.

The ghost girl's assault proved futile, and the Kitsune made off with Finn. She pursued them, but she couldn't close the distance in time.

  
  

  
  

Finn was in pure bliss, his body enveloped in the warmest cocoon, akin to returning to his mother's womb. The fragrance surrounding him was heavenly, and a bewitchingly beautiful melody filled his ears.

He found himself in a laboratory, modern and pristine, well-lit, with neatly arranged vials of colorful liquid on the desk. The tubes throughout the room emitted a soft, pale pink glow. Each tube contained a human body; at first glance, they appeared normal, but most displayed extra limbs, enhanced muscles, or some other peculiar alteration.

There was one particular tube at the center of the room, holding the naked body of a teenage girl, approximately 16 years old. Her face bore a striking resemblance to Ava, Finn's older sister.

The Kitsune was singing—a sign that her prey was about to become her meal. Her warm smile masked the loving demeanor of a mother, but her blood-soaked appearance and sharp fangs betrayed her intent.

Finn began to stir. A Kitsune's song had a captivating, irresistible charm that would awaken its prey. What the Kitsune loved most was seeing her prey suffer while she tortured and consumed their liver.

Finn initially felt annoyed at being awakened from his pleasant dream by the song, but as he listened, he became infatuated with the Nine-Tailed Fox's voice and allure.

The fox woman slid her long fangs across his face, scratching him. Surprisingly, this gesture brought pleasure rather than pain. Her fangs then moved down to his stomach, where she slowly ran her fingers over it, easily tearing through the thick fabric of his security uniform to reveal his naked belly.

With a swift and deliberate motion, she slashed at his stomach. Her fangs cut the skin slowly and deeply, causing unbearable pain. Blood gushed from the wound, revealing Finn's internal organs, which were submerged in a pool of crimson.

As fate would have it, at that very moment, the werewolf entered the lab. Upon seeing Finn, the Kitsune swiftly climbed into a vent and fled as far as possible.

The werewolf's face contorted with confusion, but it didn't take long for his bewilderment to deepen.

"Finnnnnnnnnn!" The ghost girl, tears streaming down her face, entered the room with a glowing lime-colored sword. With a fierce cry, she swung the blade at the werewolf.

  
  

  
  

A shimmering heat haze emanated from the green sword, but she couldn't maintain her grip on it due to her ghostly form. Instead, she swiftly possessed a cargo-moving robot that happened to be in the facility. The wolf-man dodged her slashing attack by leaning backward, and he swiftly moved to the opposite side of the room from Finn.

Despite the excruciating pain he was in, Finn managed to tear his clothes and clumsily bind them around his body, which brought some relief. As he staggered backward, he stumbled into a desk, causing a flask to topple over and spill its contents onto his wound. The liquid soaked into the cloth around him, and he felt the wound burn hot as the bleeding stopped. Weakened but determined, he turned his head to glance at a computer on the desk. A red button was visible, though it was partially covered by a case. Leaning on the desk, he pushed through the pain, watching the fierce battle between the ghost and the werewolf.

Swift slashes flew through the air, but the werewolf agilely evaded every attack, not even a hair on his fur being grazed. He made his way to a wall locker and retrieved an equally impressive weapon—a silver sword designed to purify ghosts.

Sensing impending danger, the ghost girl grew increasingly desperate. Her slashes quickened, and her movements became more frenzied. But the werewolf drew closer, managing to touch her ghostly form, causing her to stutter and glitch while retreating.

Seeing his prey cornered, the werewolf closed in, trapping her in a corner. The ghost girl, desperately searching for a way to turn the situation around, noticed the prominent red button on the desk next to Finn.

"Finn, press the red button!" she shouted as loudly as she could.

In response, the werewolf leaped toward Finn, but it was too late—the button had already been pressed.

The ghost girl wasted no time. She plunged the sword into the werewolf's back and, using the battered robot, seized him and made a run for it. Explosions resonated from all around, with each passing second bringing a new eruption that drew nearer and nearer.

Spotting the exit hatch, she used the robot to propel Finn toward it, but just as she did, an explosion erupted right beside them.

In the serene, green park, morning light bathed the surroundings. A hatch was visible beneath the fountain at the park's center, drawing a gathering of people, with police officers scattered throughout.

  
  

Lying on the ground was Finn's lifeless body, his breath extinguished. Beside him, his father, displaying wolf-like features such as ears, tails, and fangs, also lay in a state of death. Clutched in his father's hand was a journal, adorned with a photo of Ava.

The following day, Finn regained consciousness, finding himself cradled in the lap of the ghost girl. It felt like a deja vu.

"I brought you back again, didn't I?" the girl spoke to him.

Finn's confusion was palpable as he gazed at her beautiful face. He extended his hand to touch her, but it passed right through her ethereal form. More surprisingly, his own hands were composed of a semi-transparent substance.

"Did I save you this time as well?" Finn replied after a moment of reflection.

"Sure, do you want a reward or something?" the girl asked, her smile warm.

Finn grinned. "I do. How about you tell me your name?"

She met his request with a smile and answered, "I am Luna."