# Ariel's World.

This is my world, a name i made because "capsule" was taken as a repo on my codeberg acccount. It means a website/capsule/watever that lives in multiple protocols.

It *Will* have 2 parts:
1. The Main site
This will be my main blog, where i post stuff.
2. The Mirage Order site
Here i will roleplay, something like how SCP works.

## How to build
To build my world, you should have:
- [gemgen](https://sr.ht/~kota/gemgen/)
- [pandoc](https://pandoc.org/)

If using Nix or nixos you can enter a dev shell if you 
have flakes enabled with:
```
$ nix develop
```

Create a `conf.json` file in the repo:
```
{
	"src":"capsules",
	"gmi":"/tmp/gmi",
	"xhtml":"/tmp/xhtml"
}
```

Build with:
```
$ ./convert
```
## Git P2P
I also will support that git based online-first gemini way of reading, when a agreement is made on it.


- [Low Buget P2P distributio with git by Solderpunk](gemini://zaibatsu.circumlunar.space/~solderpunk/gemlog/low-budget-p2p-content-distribution-with-git.gmi)
- [Geminispace Thread](gemini://bbs.geminispace.org/s/Gemini/6478)
- [Gwit Spec Capsule](gemini://oldest.gwit.site/)


